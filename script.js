//Task 1
// function fetchAlbums() {
//  fetch('https://jsonplaceholder.typicode.com/users/1/albums')
//  .then((response) => {
//    if (!response.ok) {
//      throw new Error(`Failed with status code: ${response.status}`);
//    }
//    return response.json();
//  })
//  .then((data) => {
//    console.log("Result: ", data);
//  })
//  .catch((error) => {
//    console.log("Request Error: ", error);
//  });
// }

// fetchAlbums();

async function fetchAlbums() {
  try {
    const response = await fetch(
      'https://jsonplaceholder.typicode.com/users/1/albums'
    );
    const data = await response.json();
    console.log("Result: ", data);
  } catch (err) {
    console.log(err);
  }
}

fetchAlbums();

//Task 2
async function getPlanets() {
  try {
    const response = await fetch(
      'https://swapi.py4e.com/api/planets/'
    );
    const data = await response.json();
    console.log("Result: ", data.results);
  } catch (err) {
    console.log(err);
  }
}

getPlanets();

//Task 3

async function getSkywalkers() {
  try {
    const response = await fetch(
      'https://swapi.py4e.com/api/people/?search=Skywalker'
    );
    const data = await response.json();
    console.log("Result: ", data.results);
  } catch (err) {
    console.log(err);
  }
}
getSkywalkers();

//Task 4
async function fetchSWAPI(resource, throwError = false) {
  let rootUrl = 'https://swapi.py4e.com/api/';
  if (resource.includes(rootUrl)) {
    rootUrl = '';
  }
  try {
    const response = await fetch(`${rootUrl}${resource}`);
    if (!response.ok) {
      throw new Error(`Failed with status code: ${response.status}`)
    }
    const data = await response.json();
    return data;
  } catch (error) {
    console.log("fetchSWAPI error ", error);
    if (throwError == true) {
    throw error;
    }
  } 
}

async function testFetchSWAPI() {
  try {
    const person = await fetchSWAPI("people/1/");
    console.log("person ", person);

    const film = await fetchSWAPI("https://swapi.py4e.com/api/films/1/");
    console.log("film ", film);

    const film1001Id = await fetchSWAPI("films/1001/");
    console.log("film1001Id ", film1001Id);

    // should throw error
    await fetchSWAPI("films/1101/", true);
  } catch (error) {
    console.log("testFetchSWAPI error ", error);
  }
}

testFetchSWAPI();

//Task 5
async function getPersonFilms(name) {
  try {
    const urlPerson = `people/?search=${name}`;
    const person = await fetchSWAPI(urlPerson);
    const films = person.results[0].films;
    const personFilms = await Promise.all(films.map(url => fetchSWAPI(url)));
    return {name: person.results[0].name, films: personFilms};
    //return personFilms;
  } catch (error) {
    console.log('getPersonFilm', error);
  }
}

async function testGetPersonFilms() {
  const lukeFilms = await getPersonFilms("Luke");
  console.log("lukeFilms ", lukeFilms);

  const kenobiFilms = await getPersonFilms("Kenobi");
  console.log("kenobiFilms ", kenobiFilms);
}

testGetPersonFilms();
